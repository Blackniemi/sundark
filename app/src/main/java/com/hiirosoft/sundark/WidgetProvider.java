package com.hiirosoft.sundark;

import android.app.KeyguardManager;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.graphics.RectF;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Debug;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.widget.AnalogClock;
import android.widget.RemoteViews;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

public class WidgetProvider extends AppWidgetProvider {

    private static final String LOG_TAG = "com.hiirosoft.SunDark";

    private static final int TIMEZONE_UNDEFINED = 99;
    private static final double TIME_UNDEFINED = 99;

    private static final double LAT_HELSINKI = 60.1699;
    private static final double LON_HELSINKI = 24.9384;
    private static final double LAT_BAKER_LAKE = 64.3176;
    private static final double LON_BAKER_LAKE = -96.0220;

    private double riseT;
    private double setT;



    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        super.onUpdate(context, appWidgetManager, appWidgetIds);

        for (int appWidgetId : appWidgetIds) {
            updateAppWidget(context, appWidgetManager, appWidgetId);
        }
    }
    @Override
    public void onEnabled(Context context) {
        context.registerReceiver(new PhoneUnlockedReceiver(), new IntentFilter(Intent.ACTION_BOOT_COMPLETED));
        Log.v(LOG_TAG, "####DEBUGGIA10");
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals(Intent.ACTION_SCREEN_ON)) {
            Log.v(LOG_TAG, "DEBUGGIA");
        }
        if (intent.getAction().equals(Intent.ACTION_USER_PRESENT)) {
            Log.v(LOG_TAG, "DEBUGGIA2");
        }
        if (intent.getAction().equals(Intent.ACTION_TIME_TICK)) {
            Log.v(LOG_TAG, "DEBUGGIA4");
        }

        KeyguardManager keyguardManager = (KeyguardManager)context.getSystemService(Context.KEYGUARD_SERVICE);
        if (keyguardManager.isKeyguardSecure()) {

            //phone was unlocked, do stuff here
            Log.v(LOG_TAG, "DEBUGGIA3");
        }
    }

    void updateAppWidget(Context context, AppWidgetManager appWidgetManager,
                                int appWidgetId) {
        Intent intent = new Intent(context, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, 0);

        RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.sundark_widget_layout);
        views.setOnClickPendingIntent(R.id.sundark_widget_bg, pendingIntent);

        /* Permission checks */
        if ( Build.VERSION.SDK_INT >= 23 &&
                ContextCompat.checkSelfPermission(context, android.Manifest.permission.ACCESS_FINE_LOCATION ) != PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission( context, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            return; // No permissions granted, what do?
        }
        else
        {
            LocationManager locationManager = (LocationManager)context.getSystemService(Context.LOCATION_SERVICE);
            Location location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);

            if (location == null)
            {
                location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            }

            if (location != null)
            {
                Log.e(LOG_TAG, "####SUNDARK_INFO: Location acquired.");
                SolveSunTimes(location.getLatitude(), location.getLongitude());
            }
            else
            {
                Log.e(LOG_TAG, "ERROR: Could not resolve location.");
            }
        }


        Paint paint = new Paint();
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(Color.YELLOW);
        paint.setTextSize(24);

        Bitmap.Config conf = Bitmap.Config.ARGB_8888;
        Bitmap arcBitmap = Bitmap.createBitmap(512, 512, conf);
        Canvas canvas = new Canvas(arcBitmap);

        //canvas.drawText("rise: " + String.format("%.2f", riseT), arcBitmap.getScaledWidth(canvas)/2 -40, arcBitmap.getScaledHeight(canvas)/2 -20, paint);
        //canvas.drawText("set: " + String.format("%.2f", setT), arcBitmap.getScaledWidth(canvas)/2 -40, arcBitmap.getScaledHeight(canvas)/2 + 20, paint);

        /* Arc paint configuration */
        Paint arcPaint = new Paint();
        arcPaint.setStyle(Paint.Style.STROKE);
        //arcPaint.setStrokeCap(Paint.Cap.BUTT);
        arcPaint.setColor(Color.YELLOW);
        arcPaint.setStrokeWidth(25f);

        Point p1 = new Point(arcBitmap.getScaledWidth(canvas)/2, arcBitmap.getScaledHeight(canvas)/2);

        if (setT - 12 > riseT) /* DRAW TWO ARCS */
        {
            if (riseT < 12)
            {
                double startAngleRad = (riseT / 12) * 2*Math.PI - Math.PI/2;
                double endAngleRad;
                if (setT >= 12)
                {
                    endAngleRad = 2*Math.PI - Math.PI/2;
                }
                else
                {
                    endAngleRad = (setT / 12) * 2*Math.PI - Math.PI/2;
                }
                double startAngle = Math.toDegrees(startAngleRad);
                double sweepAngle = Math.toDegrees(endAngleRad - startAngleRad);
                float radius = arcBitmap.getScaledHeight(canvas)/2 * 0.75f;
                final RectF oval = new RectF();
                oval.set(p1.x - radius, p1.y - radius, p1.x + radius, p1.y + radius);
                Path path = new Path();
                path.arcTo(oval, (float)startAngle, (float)sweepAngle, true);

                canvas.drawPath(path, arcPaint);
                views.setImageViewBitmap(R.id.sundark_arc_outer, arcBitmap);
            }

            if (setT > 12)
            {
                double endAngleRad = (setT / 12) * 2*Math.PI - Math.PI/2;
                double startAngleRad;
                if (riseT >= 12)
                {
                    startAngleRad = 2*Math.PI - Math.PI/2;
                }
                else
                {
                    startAngleRad = 0f - Math.PI/2;
                }
                double startAngle = Math.toDegrees(startAngleRad);
                double sweepAngle = Math.toDegrees(endAngleRad - startAngleRad);
                float radius = arcBitmap.getScaledHeight(canvas)/2 * 0.65f;
                final RectF oval = new RectF();
                oval.set(p1.x - radius, p1.y - radius, p1.x + radius, p1.y + radius);
                Path path = new Path();
                path.arcTo(oval, (float)startAngle, (float)sweepAngle, true);

                canvas.drawPath(path, arcPaint);
                views.setImageViewBitmap(R.id.sundark_arc_inner, arcBitmap);
            }
        }
        else /* SINGULAR ARC */
        {
            if (setT > 12)
            {
                double endAngleRad = (setT / 12) * 2*Math.PI - Math.PI/2;
                double startAngleRad;
                if (riseT >= 12)
                {
                    startAngleRad = 2*Math.PI - Math.PI/2;
                }
                else
                {
                    startAngleRad = 0f - Math.PI/2;
                }
                double startAngle = Math.toDegrees(startAngleRad);
                double sweepAngle = Math.toDegrees(endAngleRad - startAngleRad);
                float radius = arcBitmap.getScaledHeight(canvas)/2 * 0.7f;
                final RectF oval = new RectF();
                oval.set(p1.x - radius, p1.y - radius, p1.x + radius, p1.y + radius);
                Path path = new Path();
                path.arcTo(oval, (float)startAngle, (float)sweepAngle, true);

                canvas.drawPath(path, arcPaint);
                views.setImageViewBitmap(R.id.sundark_arc_inner, arcBitmap);
            }

            if (riseT < 12)
            {
                double startAngleRad = (riseT / 12) * 2*Math.PI - Math.PI/2;
                double endAngleRad;
                if (setT >= 12)
                {
                    endAngleRad = 2*Math.PI - Math.PI/2;
                }
                else
                {
                    endAngleRad = (setT / 12) * 2*Math.PI - Math.PI/2;
                }
                double startAngle = Math.toDegrees(startAngleRad);
                double sweepAngle = Math.toDegrees(endAngleRad - startAngleRad);
                float radius = arcBitmap.getScaledHeight(canvas)/2 * 0.7f;
                final RectF oval = new RectF();
                oval.set(p1.x - radius, p1.y - radius, p1.x + radius, p1.y + radius);
                Path path = new Path();
                path.arcTo(oval, (float)startAngle, (float)sweepAngle, true);

                canvas.drawPath(path, arcPaint);
                views.setImageViewBitmap(R.id.sundark_arc_outer, arcBitmap);
            }
        }


        /* CLOCK HANDS */

        Paint clock_hands_paint = new Paint();
        clock_hands_paint.setColor(context.getResources().getColor(R.color.orange));
        clock_hands_paint.setColor(Color.WHITE);
        clock_hands_paint.setStrokeWidth(10f);
        clock_hands_paint.setStyle(Paint.Style.STROKE);
        clock_hands_paint.setStrokeCap(Paint.Cap.ROUND);

        double hourHandLength = 100f;
        double minuteHandLength = 200f;

        /* Minute hand */
        int minute = Calendar.getInstance().get(Calendar.MINUTE);
        double minuteRad = ((float)minute/60) * 2*Math.PI - Math.PI/2;
        float minuteEndX = (float)(minuteHandLength * Math.cos(minuteRad)) + p1.x;
        float minuteEndY = (float)(minuteHandLength * Math.sin(minuteRad)) + p1.y;
        float minuteStartX = (float)(0.25f*hourHandLength * Math.cos(minuteRad + Math.PI)) + p1.x;
        float minuteStartY = (float)(0.25f*hourHandLength * Math.sin(minuteRad + Math.PI)) + p1.y;

        /* Hour hand */
        int hour = Calendar.getInstance().get(Calendar.HOUR);
        double hourRad = ((float)hour/12) * 2*Math.PI - Math.PI/2 + (minuteRad + Math.PI/2)/12;
        float hourEndX = (float)(hourHandLength * Math.cos(hourRad)) + p1.x;
        float hourEndY = (float)(hourHandLength * Math.sin(hourRad)) + p1.y;
        float hourStartX = (float)(0.25f*hourHandLength * Math.cos(hourRad + Math.PI)) + p1.x;
        float hourStartY = (float)(0.25f*hourHandLength * Math.sin(hourRad + Math.PI)) + p1.y;

        canvas.drawLine(hourStartX, hourStartY, hourEndX, hourEndY, clock_hands_paint);
        views.setImageViewBitmap(R.id.sundark_clock_hands, arcBitmap);

        canvas.drawLine(minuteStartX, minuteStartY, minuteEndX, minuteEndY, clock_hands_paint);
        views.setImageViewBitmap(R.id.sundark_clock_hands, arcBitmap);

        appWidgetManager.updateAppWidget(appWidgetId, views);
    }


    /**
     * Calculates sunrise and sunset in local time given latitude, longitude, and tz.
     *
     * Equations taken from:
     * https://en.wikipedia.org/wiki/Julian_day#Converting_Julian_or_Gregorian_calendar_date_to_Julian_Day_Number
     * https://en.wikipedia.org/wiki/Sunrise_equation#Complete_calculation_on_Earth
     *
     * @method suntimes
     * @param {double} lat Latitude of location (South is negative)
     * @param {double} lng Longitude of location (West is negative)
     * @param {Integer} tz Timezone hour offset. e.g. Pacific/Los Angeles is -8 (Optional, defaults to system timezone)
     * @return {Array} Returns array of length 2 with sunrise and sunset as floats.
     *                 Returns array with [null, -1] if the sun never rises, and [-1, null] if the sun never sets.
     */
    private double[] suntimes(double lat, double lng, int tz) {
        Date d = new Date();
        double radians = Math.PI / 180.0;
        double degrees = 180.0 / Math.PI;

        int month = Calendar.getInstance().get(Calendar.MONTH);

        double a = Math.floor((14 - month + 1.0) / 12);
        double y = Calendar.getInstance().get(Calendar.YEAR) + 4800 - a;
        double m = (month + 1) + 12 * a - 3;
        double j_date = Calendar.getInstance().get(Calendar.DAY_OF_MONTH) + Math.floor((153 * m + 2)/5) + 365 * y + Math.floor(y/4) - Math.floor(y/100) + Math.floor(y/400) - 32045;
        double n_star = j_date - 2451545.0009 - lng / 360.0;
        double n = Math.floor(n_star + 0.5);
        double solar_noon = 2451545.0009 - lng / 360.0 + n;
        double M = 356.0470 + 0.9856002585 * n;
        double C = 1.9148 * Math.sin( M * radians ) + 0.02 * Math.sin( 2 * M * radians ) + 0.0003 * Math.sin( 3 * M * radians );
        double L = ( M + 102.9372 + C + 180 ) % 360;
        double j_transit = solar_noon + 0.0053 * Math.sin( M * radians) - 0.0069 * Math.sin( 2 * L * radians );
        double D = Math.asin( Math.sin( L * radians ) * Math.sin( 23.45 * radians ) ) * degrees;
        double cos_omega = ( Math.sin(-0.83 * radians) - Math.sin( lat * radians ) * Math.sin( D * radians ) ) / ( Math.cos( lat * radians ) * Math.cos( D * radians ) );

        // sun never rises
        if( cos_omega > 1)
            return new double[] {TIME_UNDEFINED, -1};

        // sun never sets
        if( cos_omega < -1 )
            return new double[] {-1, TIME_UNDEFINED};

        // get times
        double omega = Math.acos( cos_omega ) * degrees;
        double j_set = j_transit + omega / 360.0;
        double j_rise = j_transit - omega / 360.0;
        double delta_j_set = j_set - j_date;
        double delta_j_rise = j_rise - j_date;
        double tz_offset = (tz == TIMEZONE_UNDEFINED) ? -1 * d.getTimezoneOffset() / 60 : -tz;
        double local_rise = delta_j_rise * 24 + (12 - tz_offset);
        double local_set = delta_j_set * 24 + (12 - tz_offset);
        return new double[] {local_rise, local_set};
    }

    public void SolveSunTimes(double lat, double lon)
    {
        Calendar mCalendar = new GregorianCalendar();
        TimeZone mTimeZone = mCalendar.getTimeZone();
        int mGMTOffset = mTimeZone.getRawOffset();
        long offset = TimeUnit.HOURS.convert(mGMTOffset, TimeUnit.MILLISECONDS);
        String offset_str = Long.toString(offset);

        double[] times = suntimes(lat, lon, (int)offset); // Helsinki
        riseT = times[0];
        setT = times[1];

        Log.v(LOG_TAG, "####SUNDARK_INFO:  LAT: " + Double.toString(lat) + "    LON: " + Double.toString(lon) + "    GMT offset is " + offset_str + " hours");
        Log.v(LOG_TAG, "####SUNDARK_INFO:  RISETIME: " + Double.toString(riseT) + "  SETTIME: " + Double.toString(setT));
    }
}
