package com.hiirosoft.sundark;

import android.app.KeyguardManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class PhoneUnlockedReceiver extends BroadcastReceiver {

    private static final String LOG_TAG = "com.hiirosoft.SunDark";

    @Override
    public void onReceive(Context context, Intent intent) {

        if (intent.getAction().equals(Intent.ACTION_SCREEN_ON)) {
            Log.v(LOG_TAG, "DEBUGGIA");
        }
        if (intent.getAction().equals(Intent.ACTION_USER_PRESENT)) {
            Log.v(LOG_TAG, "DEBUGGIA2");
        }
        if (intent.getAction().equals(Intent.ACTION_TIME_TICK)) {
            Log.v(LOG_TAG, "DEBUGGIA4");
        }
        KeyguardManager keyguardManager = (KeyguardManager)context.getSystemService(Context.KEYGUARD_SERVICE);
        if (keyguardManager.isKeyguardSecure()) {

            //phone was unlocked, do stuff here
            Log.v(LOG_TAG, "DEBUGGIA3");

        }
    }
}
